
#[cfg(test)]


#[test]
fn process() {
    use process::process::Process;

    let mut process = Process::new("/Users/schuepbs/Documents/Projects/saana-backend/tools/stdio.sh".to_string());
    process.start();
    let res  = process.read();
    assert_eq!(res, Some("1 0".to_string()));
}