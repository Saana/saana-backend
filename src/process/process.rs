use std::process::{Command, Stdio};
use std::process::Child;
use std::str;

use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug)]
pub struct Process{

    file : String,
    child : Option<Child>,
}

impl Process{

    pub fn new(file : String) -> Process{
        Process{
            file : file,
            child : None,
        }
    }

    pub fn parse_to_int(data : String) -> Option<(u32,u32)>{

        let splitted: Vec<&str> = data.split_whitespace().collect();

        let card = match splitted.get(0) {
            Some(d) => match d.parse::<u32>(){
                Ok(v) => Some(v),
                Err(e) => {println!("{}", e.to_string()); None}
            },
            None => None,
        };

        let suit = match splitted.get(1) {
            Some(d) => match d.parse::<u32>(){
                Ok(v) => Some(v),
                Err(e) => {println!("{}", e.to_string()); None}
            },
            None => None,
        };

        match card {
            Some(c) => match suit {
                Some(s) => Some((c, s)),
                None => {
                    println!("Couldn't parse cards to int");
                    None
                },
            },
            None => {
                println!("Couldn't parse cards to int");
                None
            },
        }
    }

    pub fn start(&mut self){
      //  println!("Starting Process: {}", self.file.as_str());
        self.child = Some(Command::new(self.file.as_str())
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .unwrap());
    }

    pub fn read(&mut self) -> Option<String>{
        match self.child {
            Some(ref mut child) => match child.stdout {
                Some(ref mut stdout) => {

                    let mut child_out = BufReader::new(stdout);

                    let mut input_text = String::new();

                    child_out.read_line(&mut input_text).unwrap();

                    Some(input_text)
                },
                None => {println!("Couldn't find input"); None},
            }
            None => {println!("Couldn't find input"); None},
        }
    }

    pub fn stop(&mut self){
        match self.child.take(){
            Some(mut child) => child.kill().unwrap(),
            None => println!("Process wasn't started"),
        }
    }

}



