#[cfg(test)]

use state_machine::state_machine;
use state_machine::state_machine::Executions;

pub struct FsmTest{

    pub is_running : bool,
    pub is_next : bool,
    pub team_numb : u32,
}

impl Executions for FsmTest {

    fn start (&mut self){
        self.is_running = true;
    }

    fn next (&mut self){
        self.is_next = true;
    }

    fn stop (&mut self){
        self.is_running = false;
        self.is_next = false;
    }

    fn idle (&mut self){
        self.team_numb = 0;
    }

    fn first_team (&mut self){
        self.team_numb = 1;
    }

    fn second_team (&mut self){
        self.team_numb = 2;
    }

    fn failure (&mut self){
        println!("State machine failure")
    }
}


#[test]
fn run_machine() {

    let mut parent = FsmTest{is_running : false, is_next : false, team_numb : 0};

    let mut fsm = state_machine::Machine::new();

    fsm.next(state_machine::FsmEvent::Start, &mut parent);

    assert_eq!(parent.is_running, true);

    fsm.run(&mut parent);

    assert_eq!(parent.team_numb, 1);

    fsm.next(state_machine::FsmEvent::Next, &mut parent);

    assert_eq!(parent.is_next, true);

    fsm.run(&mut parent);

    assert_eq!(parent.team_numb, 2);

    fsm.next(state_machine::FsmEvent::Stop, &mut parent);

    assert_eq!(parent.is_running, false);

}

#[test]
fn get_events() {

    let event_start = state_machine::Machine::match_event_state("start");
    match event_start {
        Some(event) => assert_eq!(event, state_machine::FsmEvent::Start),
        None => assert!(false),
    }

    let event_stop = state_machine::Machine::match_event_state("stop");
    match event_stop {
        Some(event) => assert_eq!(event, state_machine::FsmEvent::Stop),
        None => assert!(false),
    }

    let event_next= state_machine::Machine::match_event_state("next");
    match event_next{
        Some(event) => assert_eq!(event, state_machine::FsmEvent::Next),
        None => assert!(false),
    }

    let event_none = state_machine::Machine::match_event_state("skjd");
    match event_none {
        Some(_) => assert!(false),
        None => assert!(true),
    }
}