#[derive(Debug, PartialEq, Copy, Clone)]
enum State {
    Idle,
    FirstTeam,
    SecondTeam,
    Failure,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum FsmEvent {
    Start,
    Next,
    Stop,
}

pub trait Executions {

    fn start (&mut self);
    fn next (&mut self);
    fn stop (&mut self);
    fn idle (&mut self);
    fn first_team (&mut self);
    fn second_team (&mut self);
    fn failure (&mut self);

}

pub struct Machine {
    state : State,
}

impl Machine {

    pub fn new() -> Machine{
        Machine{
            state : State::Idle,
        }
    }

    pub fn next<T : Executions>(&mut self, event: FsmEvent, parent : &mut T) {

        let state = self.state;

        match (state, event) {
            (State::Idle, FsmEvent::Start) => {
                parent.start();
                self.state = State::FirstTeam;
            }

            (State::FirstTeam, FsmEvent::Next) => {
                parent.next();
                self.state = State::SecondTeam;
            }

            (State::SecondTeam,  FsmEvent::Next) => {
                parent.stop();
                self.state = State::Idle;
            }

            (_s,  FsmEvent::Stop) => {
                parent.stop();
                self.state = State::Idle;
            }

            (_s, _e) => {

            }
        }
    }

    pub fn run<T : Executions>(&mut self, parent : &mut  T) {
        match self.state {
            State::Idle => {
                parent.idle();
            },
            State::FirstTeam => {
                parent.first_team();
            }
            State::SecondTeam => {
                parent.second_team();
            }
            State::Failure => {
                parent.failure();
            }
        }
    }

    pub fn match_event_state (event : &str) -> Option<FsmEvent> {
        match event {

            "start" => Some(FsmEvent::Start),
            "stop" => Some(FsmEvent::Stop),
            "next" => Some(FsmEvent::Next),
            _ => None,
        }
    }
}
