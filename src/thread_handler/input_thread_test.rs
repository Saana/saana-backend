#[cfg(test)]
use thread_handler::input_thread;
use thread_handler::input_thread::Threadable;
use observer::observer::Observer;
use observer::observer::Event;
use std::sync::mpsc::{Receiver, Sender};

#[derive(Debug, Clone)]
struct ThreadFunction{
    pub tx : Sender<String>,
}

impl Threadable for ThreadFunction {

    fn pass_tx(&mut self, tx : Sender<String>){
        self.tx = tx;
    }

    fn run(&mut self) -> Event {

        match self.tx.send("Test".to_string()) {
            Ok(_) => {},
            Err(e) => {println!("Couldn't send: {}", e.to_string())},
        }
        Event::StdIo
    }
}

#[derive(Debug)]
struct Rec {
    pub data : String,
    pub rx : Receiver<String>,
}

impl Observer for Rec {
    fn update(&mut self, _event: &Event){
         match self.rx.recv() {
            Ok(x) => {self.data = x},
            Err(e) => {panic!("{}", e.to_string())},
        };
    }
}

#[test]
fn test_thread_notification() {

    use observer::observer::Dispatchable;
    use std::sync::{Mutex, Arc};
    use std::sync::mpsc;
    use observer::observer::ThreadDispatcher;
    use thread_handler::input_thread::InputThread;


    let (tmp_tx, tmp_rx) = mpsc::channel();

    let rec = Arc::new(Mutex::new(Rec{data : "".to_string(),rx  : tmp_rx}));

    let fun = Arc::new(Mutex::new(ThreadFunction{tx:tmp_tx}));

    let dispatcher = Arc::new(Mutex::new(ThreadDispatcher::new()));
    dispatcher.lock().unwrap().register(rec.clone());


    let mut thread = InputThread::new(dispatcher, fun, "test".to_string());

    match rec.lock(){
        Ok(mut guard) => guard.rx = thread.start(),
        Err(e) => panic!("{}", e.to_string()),
    }

    thread.stop();

    let rec_data = match rec.lock() {
        Ok(x) => x.data.clone(),
        Err(e) => panic!("{}", e.to_string()),
    };

    assert_eq!("Test".to_string(), rec_data);

}
