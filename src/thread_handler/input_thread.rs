use observer::observer::ThreadDispatcher;
use observer::observer::Observer;
use std::thread;
use std::sync::mpsc;
use std::sync::{Mutex, Arc};
use std::sync::mpsc::Sender;
use std::marker::Send;
use observer::observer::Dispatchable;
use observer::observer::Event;

/*
U : updates observer
T : functionality to receive data
*/

pub struct InputThread <T, U> {

    dispatcher : Arc<Mutex<ThreadDispatcher<U>>>,
    ctrl_tx : Sender<bool>,
    msg_tx : Sender<String>,
    name : String,
    thread_fun : Arc<Mutex<T>>,
    thread_han : Option<thread::JoinHandle<()>>,
}

pub trait Threadable {
    fn run(&mut self) -> Event;
    fn pass_tx(&mut self, tx : Sender<String>);
}

impl <T, U> InputThread <T, U>
    where T : Threadable + Send + 'static,
          U : Observer + Send + 'static{

    pub fn new(dispatcher : Arc<Mutex<ThreadDispatcher<U>>>,
               thread_fun : Arc<Mutex<T>>,
               name : String) -> InputThread<T, U>  {

        let (tx_0, _rx_0) = mpsc::channel();
        let (tx_1, _rx_1) = mpsc::channel();

        InputThread{
            name : name,
            dispatcher : dispatcher,
            ctrl_tx : tx_1,
            msg_tx : tx_0,
            thread_fun : thread_fun,
            thread_han : None,
        }
    }

    pub fn start(&mut self){

        let (ctrl_tx, ctrl_rx) = mpsc::channel();

        self.ctrl_tx = ctrl_tx;

        let thread_fun_mutexed = self.thread_fun.clone();
        let dispatcher_mutexed = self.dispatcher.clone();

        let thread = thread::Builder::new().name("input-handler-thread".to_string()).spawn(move || {

            let mut is_running = true;

            while is_running == true {

                let ev = match thread_fun_mutexed.lock() {
                    Ok(mut thread_fun) => thread_fun.run(),
                    Err(e) => panic!("{}", e.to_string()),
                };

                match dispatcher_mutexed.lock() {
                    Ok(mut dispatcher) => dispatcher.notify(&ev),
                    Err(e) => panic!("{}", e.to_string()),
                }

                is_running = match ctrl_rx.try_recv() {
                    Ok(x) => x,
                    Err(_) => true,
                };
            }
        }).unwrap();
        self.thread_han = Some(thread);
    }

    pub fn stop(&mut self){
        let send_stop = false;
        match self.ctrl_tx.send(send_stop) {
            Ok(_x) => {},
            Err(e) => panic!("{}", e.to_string()),
        }

        if let Some(thread) = self.thread_han.take() {
            match thread.join() {
                Ok(_x) => info!("Thread stopped"),
                Err(_y) => warn!("Thread couldn't be joined"),
            };
        }
    }
}