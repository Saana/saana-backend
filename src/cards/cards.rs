const POINTS_NORMAL : &'static [u32] = &[0, 0, 0, 0, 10, 2, 3, 4, 11];
const POINTS_DOWNUP : &'static [u32] = &[0, 0, 8, 0, 10, 2, 3, 4, 11];
const POINTS_UPDOWN : &'static [u32] = &[11, 0, 8, 0, 10, 2, 3, 4, 0];
const POINTS_TRUMP : &'static [u32] = &[0, 0, 0, 14, 10, 20, 3, 4, 11];

#[derive(Clone, Copy, Debug)]
pub struct CardConf{
    pub mode : u32,
    pub trump : u32,
}

#[derive(Clone, Copy, Debug)]
enum Modes{
    Trump(u32),
    Updown,
    Downup,
    None,
}

#[derive(Clone, Copy, Debug)]
pub struct Cards{
    mode : Modes,
}

trait Points{
    fn get_points(&self, card : usize, suit : usize) -> Option<u32>;
}

impl Cards {

    pub fn new() -> Cards{
        Cards{
            mode : Modes::None,
        }
    }

    pub fn set_config(&mut self, settings : CardConf){

        println!("Set game to mode: {}", settings.mode);

        self.mode = match settings.mode {
            0 => Modes::Trump(settings.trump),
            1 => Modes::Updown,
            2 => Modes::Downup,
            _ => Modes::None,
        };
    }

    pub fn get_points(&self, card : u32, suit : u32) -> Option<u32>{

        match self.mode {
            Modes::Trump(trump) => Trump{trump : trump}.get_points(card as usize, suit as usize),
            Modes::Downup => Downup{}.get_points(card as usize, suit as usize),
            Modes::Updown => Updown{}.get_points(card as usize, suit as usize),
            Modes::None => None,
        }
    }
}

#[derive(Clone, Copy)]
struct Downup;
impl Points for Downup{
    fn get_points(&self, card : usize, _suit : usize) -> Option<u32>{
        fetch_points(POINTS_DOWNUP, card)
    }
}

#[derive(Clone, Copy)]
struct Updown;
impl Points for Updown{
    fn get_points(&self, card : usize, _suit : usize) -> Option<u32>{
        fetch_points(POINTS_UPDOWN, card)
    }
}

#[derive(Clone, Copy)]
struct Trump{
    trump : u32,
}
impl Points for Trump{
    fn get_points(&self, card : usize, suit : usize) -> Option<u32>{
        if suit as u32 == self.trump {
            fetch_points(POINTS_TRUMP, card)
        } else {
            fetch_points(POINTS_NORMAL, card)
        }
    }
}

fn fetch_points(points : &[u32], card : usize) -> Option<u32> {
    match points.get(card) {
        Some(x) => Some(*x),
        None => None,
    }
}