#[cfg(test)]


#[test]
fn get_trumpf() {

    use cards::cards::Cards;
    use cards::cards::CardConf;

    let mut cards = Cards::new();
    cards.set_config(CardConf{mode : 0, trump : 1});

    let mut result = cards.get_points(5, 2);

    match result {
        Some(x) => assert_eq!(x, 2),
        None    => println!("No card found"),
    }

    result = cards.get_points(5, 1);

    match result {
        Some(x) => assert_eq!(x, 20),
        None    => println!("No card found"),
    }

    cards.set_config(CardConf{mode : 1, trump : 1});
    result = cards.get_points(0, 1);

    match result {
        Some(x) => assert_eq!(x, 11),
        None    => println!("No card found"),
    }

}
