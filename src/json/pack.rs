use serde_json::Error;
use serde_json;

pub struct Packer {}

#[derive(Serialize, Deserialize)]
pub struct JsonOutput {
    pub team : u32,
    pub card : u32,
    pub suit : u32,
    pub total : u32,
}

impl Packer {

    pub fn pack(data : JsonOutput) -> Result<String,Error> {
        serde_json::to_string(&data)
    }
}