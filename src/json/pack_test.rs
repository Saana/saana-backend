#[cfg(test)]


#[test]
fn test_unpacking() {
    use json::pack::Packer;
    use json::pack::JsonOutput;

    let ref_data = r#"{"team":0,"card":1,"suit":2,"total":200}"#.to_string();

    let data = JsonOutput{
        team : 0_u32,
        card : 1_u32,
        suit : 2_u32,
        total : 200_u32,
    };


    let output : String = match Packer::pack(data){
        Ok(d) => d,
        Err(err) => panic!(err),
    };

    assert_eq!(output, ref_data);
}