#[cfg(test)]

use json::unpack;

#[test]
fn test_unpacking() {

    use json::unpack::Unpacker;

    let data = r#"
        {
            "command": "settings",
            "data": {
                "trump": 2,
                "mode": 1,
                "lasttrick": 0
            }
        }
    "#;

    let input : unpack::JsonInput = match Unpacker::unpack(data){
        Ok(d) => d,
        Err(err) => panic!(err),
    };

    assert_eq!(input.command, "settings".to_string());
    assert_eq!(input.data.trump, 2);
    assert_eq!(input.data.mode, 1);
    assert_eq!(input.data.lasttrick, 0);
}