use serde_json::Error;
use serde_json;

#[derive(Serialize, Deserialize)]
pub struct JsonData{
    pub mode : u32,
    pub trump : u32,
    pub lasttrick : u32,
}

#[derive(Serialize, Deserialize)]
pub struct JsonInput {
    pub command : String,
    pub data : JsonData,
}

pub struct Unpacker {}

impl Unpacker{
    pub fn unpack(data : &str) -> Result<JsonInput, Error> {
        serde_json::from_str(data)
    }
}
