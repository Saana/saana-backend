#[cfg(test)]
use observer::observer;
use observer::observer::Observer;
use observer::observer::Event;
use std::sync::mpsc::Receiver;

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct ConcreteStdio{
    pub test : bool,
}

impl Observer for ConcreteStdio{

    fn update(&mut self, event : &Event){

        match *event {
            Event::StdIo => self.test = true,
            _ => {},
        }
    }
}


#[test]
fn test_observer() {

    use std::sync::{Arc};
    use std::cell::RefCell;
    use observer::observer::Dispatchable;

    let mut test_sub = observer::Dispatcher::new();

    let obs_one_rc = Arc::new(RefCell::new(ConcreteStdio{test : false}));

    test_sub.register(obs_one_rc.clone());

    assert_eq!(test_sub.len(), 1);

    test_sub.notify(&Event::StdIo);

    assert_eq!(obs_one_rc.borrow().test, true);
}


#[derive(Debug)]
pub struct ConcreteThread{

    rx : Receiver<bool>,
    pub test : bool,
}

impl ConcreteThread {
    pub fn new(rx : Receiver<bool>) -> ConcreteThread{
        ConcreteThread{
            rx : rx,
            test : false,
        }
    }
}

impl Observer for ConcreteThread{

    fn update(&mut self, event : &Event){

        match *event {
            Event::StdIo => self.test = {
                println!("State ok");
                self.rx.recv().unwrap()
            },
            _ => {println!("State wrong")},
        }
    }
}

#[test]
fn test_thread() {

    use std::sync::{Arc, Mutex};
    use std::sync::mpsc;
    use std::thread;
    use observer::observer::Dispatchable;

    let (tx, rx) = mpsc::channel();

    let mut test_sub = observer::ThreadDispatcher::new();

    let obs_one_rc = Arc::new(Mutex::new(ConcreteThread::new(rx)));
    test_sub.register(obs_one_rc.clone());


    let handle = thread::spawn(move || {
        let val = true;

        tx.send(val).unwrap();
        test_sub.notify(&Event::StdIo);

    });

    handle.join().unwrap();


    assert_eq!(obs_one_rc.lock().unwrap().test, true);
}