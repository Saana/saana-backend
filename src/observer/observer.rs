use std::sync::{Arc, Mutex};
use std::sync::Weak;
use std::cell::RefCell;

pub enum Event {
    WebSocket,
    StdIo,
    Error,
}

pub trait Dispatchable<T>{
    type RefType;
    type InternalDispatcher;

    fn register(&mut self, observer : Arc<Self::RefType>);

    fn new() -> Self::InternalDispatcher;

    fn len(&self) -> usize;

    fn notify(&mut self, event: &Event);
}

pub struct Dispatcher<T> {
    observers : Vec<Weak<RefCell<T>>>,
}

impl <T : Observer> Dispatchable<T> for Dispatcher<T> {
    type RefType = RefCell<T>;
    type InternalDispatcher = Dispatcher<T>;

    fn register(&mut self, observer : Arc<Self::RefType>) {
        self.observers.push(Arc::downgrade(&observer));
    }

    fn new() -> Self::InternalDispatcher {
        Self::InternalDispatcher{
            observers : Vec::new(),
        }
    }

    fn notify(&mut self, event: &Event){
        let mut cleanup = false;
        // Call the listeners
        for l in self.observers.iter() {
            if let Some(mut listener_rc) = l.upgrade() {
                let mut listener = listener_rc.borrow_mut();
                listener.update(event);
            } else {
                println!("Cannot get listener, cleanup necessary");
                cleanup = true;
            }
        }

        // If there were invalid weak refs, clean up the list
        if cleanup {
            println!("Dispatcher is cleaning up weak refs");
            self.observers.retain(|ref l| {
                // Only retain valid weak refs
                let got_ref = l.clone().upgrade();
                match got_ref {
                    None => false,
                    _ => true,
                }
            });
        }
    }

    fn len(&self) -> usize {
        self.observers.len()
    }
}

pub struct ThreadDispatcher<T> {
    observers : Vec<Weak<Mutex<T>>>,
}

impl <T : Observer> Dispatchable<T> for ThreadDispatcher<T> {
    type RefType = Mutex<T>;
    type InternalDispatcher = ThreadDispatcher<T>;

    fn register(&mut self, observer : Arc<Self::RefType>) {
        self.observers.push(Arc::downgrade(&observer));
    }

    fn len(&self) -> usize {
        self.observers.len()
    }

    fn new() -> Self::InternalDispatcher {
        Self::InternalDispatcher{
            observers : Vec::new(),
        }
    }

    fn notify(&mut self, event: &Event){
        let mut cleanup = false;
        // Call the listeners
        for l in self.observers.iter() {
            if let Some(mut listener_rc) = l.upgrade() {
                let mut listener = listener_rc.lock().unwrap();
                listener.update(event);
            } else {
                println!("Cannot get listener, cleanup necessary");
                cleanup = true;
            }
        }
        // If there were invalid weak refs, clean up the list
        if cleanup {
            println!("Dispatcher is cleaning up weak refs");
            self.observers.retain(|ref l| {
                // Only retain valid weak refs
                let got_ref = l.clone().upgrade();
                match got_ref {
                    None => false,
                    _ => true,
                }
            });
        }
    }
}

pub trait Observer {
    fn update(&mut self, event: &Event);
}