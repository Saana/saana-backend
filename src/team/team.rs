#[derive(Clone, Copy, Debug)]
pub struct Team {
    pub points : u32,
}

impl Team {
    pub fn add(&mut self, new_points : u32) {
        self.points = self.points + new_points;
    }
    
    pub fn reset(&mut self){
        self.points = 0_u32;
    }
    
    pub fn get(&self) -> u32{
        return self.points;
    }
}
