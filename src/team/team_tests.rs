#[cfg(test)]

use team::team;

#[test]
fn getter_setter() {
    
    let mut team = team::Team {points : 0_u32};
    team.add(5_u32);
    team.add(10_u32);
    
    assert_eq!(team.get(), 15);
}

#[test]
fn reset() {
    
    let mut team = team::Team {points : 10_u32};
    team.reset();
    
    assert_eq!(team.get(), 0_u32);
}
