use team::team;
use json::unpack::JsonInput;
use json::unpack::Unpacker;
use thread_handler::input_thread::InputThread;
use observer::observer::ThreadDispatcher;
use observer::observer::Dispatchable;
use thread_handler::input_thread::Threadable;
use std::sync::{Arc,Mutex};
use std::sync::mpsc::Sender;
use std::sync::mpsc::Receiver;
use std::sync::mpsc;
use process::process::Process;
use observer::observer::Event;
use observer::observer::Observer;
use cards::cards::Cards;
use cards::cards::CardConf;
use json::pack::Packer;
use json::pack::JsonOutput;
use std::env;

use websocket::sender::Writer;
use websocket::stream::sync::TcpStream;

use websocket::OwnedMessage;

#[derive(Clone)]
pub struct Counter {

    count : Arc<Mutex<Count>>,

    dispatcher : Arc<Mutex<ThreadDispatcher<Count>>>,
    process : Arc<Mutex<Option<Process>>>,
    thread_fun : Arc<Mutex<ThreadFunc>>,
    thread_hand : Arc<Mutex<InputThread<ThreadFunc, Count>>>,

    thread_running : bool,
}

#[derive(Clone)]
struct ThreadFunc{
    tx : Sender<String>,
    process : Arc<Mutex<Option<Process>>>,
}

impl Threadable for ThreadFunc{

    fn run(&mut self) -> Event{
        match *self.process.lock().unwrap() {
            Some(ref mut process) => {
                println!("Waiting for data from process");
                match process.read() {
                    Some(data) => {
                        match self.tx.send(data) {
                            Ok(_) => Event::StdIo,
                            Err(e) => {println!("Couldn't send data in ThreadFunc {}", e.to_string()); Event::Error}
                        }
                    }
                    None => Event::Error,
                }
            },
            None => Event::Error,
        }
    }

    fn pass_tx(&mut self, tx : Sender<String>){
        self.tx = tx;
    }
}

#[derive(Clone, Copy, Debug)]
enum CountingTeams{
    Team0,
    Team1,
}

struct Count {
    teams : (team::Team, team::Team),
    active_team : CountingTeams,
    process : Arc<Mutex<Option<Process>>>,
    rx : Receiver<String>,
    pub card : Cards,
    json_data : Option<String>,
    ip : Option<String>,
    transmitter : Option<Arc<Mutex<Writer<TcpStream>>>>,
}

impl Count{
    pub fn new(process : Arc<Mutex<Option<Process>>>, rx : Receiver<String>) -> Count {
        Count{
            teams : (team::Team {points : 0},
                     team::Team {points: 0}),
            active_team : CountingTeams::Team0,
            process : process,
            card : Cards::new(),
            rx : rx,
            json_data : None,
            ip : None,
            transmitter : None,
        }
    }

    pub fn update_websocket_transmitter(&mut self, transmitter : Arc<Mutex<Writer<TcpStream>>>){
        self.transmitter = Some(transmitter);
    }

    pub fn update_rx(&mut self, rx : Receiver<String>){
        self.rx = rx;
    }

    pub fn reset(&mut self){
        self.teams.0.reset();
        self.teams.1.reset();
    }

    fn send_update(&mut self, json_data : Option<String>){

        if let Some(data) = json_data {
            let message = OwnedMessage::Text(data);

            if let Some(ref mut transmitter) = self.transmitter{
                match transmitter.lock().unwrap().send_message(&message){
                    Ok(()) => {println!("Sent data to client")},
                    Err(e) => {
                        println!("Websocket error: {:?}", e);
                    }
                }
            } else {
                println!("No transmitter aviable");
            }
        }
    }

    pub fn update_teams(&mut self, data : String){
        let parsed = Process::parse_to_int(data);
        let points = match parsed {
            Some((card, suit)) => self.card.get_points(card, suit),
            None => {println!("Couldn't evaluate points"); None},
        };

        match points {
            Some(point) => match self.active_team {
                CountingTeams::Team0 => {
                    println!("Increment team 0 by: {}", point);
                    self.teams.0.add(point);
                    let point = self.teams.0.get();

                    if let Some((card, suit)) = parsed {
                        let json_data = Packer::pack(JsonOutput{team : 0, card : card, suit : suit, total : point}).ok();
                        self.send_update(json_data);
                    }

                },
                CountingTeams::Team1 =>  {
                    println!("Increment team 1 by: {}", point);
                    self.teams.1.add(point);
                    let point = self.teams.1.get();

                    if let Some((card, suit)) = parsed {
                        let json_data = Packer::pack(JsonOutput{team : 1, card : card, suit : suit, total : point}).ok();
                        self.send_update(json_data);
                    }
                },
            }
            None => println!("Couldn't map cards"),
        }
    }
}

impl Observer for Count{
    fn update(&mut self, event: &Event){
        match *event {
            Event::StdIo => {
                let data = self.rx.recv().expect("Couldn't receive data in Count");
                println!("Data received from process [card suit]: {}", data);
                self.update_teams(data);
            }
            _ => {},
        }
    }
}

impl Counter{

    pub fn new() -> Counter {

        let (tx, rx) = mpsc::channel();
        let dispatcher = Arc::new(Mutex::new(ThreadDispatcher::new()));

        let mut path = env::current_dir().unwrap();
        path.push("saana-core");

        let path_str = path.into_os_string().into_string().unwrap();

        let process : Arc<Mutex<Option<Process>>> = Arc::new(Mutex::new(Some(Process::new(path_str))));
        let count = Arc::new(Mutex::new(Count::new(process.clone(), rx)));

        dispatcher.lock().unwrap().register(count.clone());

        let thread_fun = Arc::new(Mutex::new(ThreadFunc{tx : tx, process : process.clone()}));
        let thread_hand = Arc::new(Mutex::new(InputThread::new(dispatcher.clone(), thread_fun.clone(), "process_thread".to_string())));

        println!("Server started");

        Counter {
            count : count,
            dispatcher : dispatcher,
            process : process,
            thread_fun : thread_fun,
            thread_hand : thread_hand,
            thread_running : false,
        }
    }


    pub fn update_websocket_transmitter(&mut self, transmitter : Arc<Mutex<Writer<TcpStream>>>){
        self.count.lock().unwrap().update_websocket_transmitter(transmitter);
    }

    pub fn get_input_data(&mut self, data : &str){

        match Unpacker::unpack(data.as_ref()){
            Ok(d) => {
                self.update_counter(d);
            },
            Err(e) => println!("No valid json: {}", e.to_string()),
        }
    }

    pub fn update_counter(&mut self, data : JsonInput){

        match data.command.as_ref() {
            "start" => {

                if self.thread_running == true {
                    println!("Thread is already running")

                } else {
                    println!("Starting process");
                    match *self.process.lock().unwrap() {
                        Some(ref mut process) => {
                            self.thread_hand.lock().unwrap().start();
                            process.start();
                            self.count.lock().unwrap().active_team = CountingTeams::Team0;
                            self.thread_running = true;
                        },
                        None => {},
                    }
                }

            },
            "next" => {
                println!("Next team");
                self.count.lock().unwrap().active_team = CountingTeams::Team1;
            },
            "stop" => {
                println!("Stopping process");
                match *self.process.lock().unwrap() {
                    Some(ref mut process) => {
                        process.stop();
                        self.thread_running = false;
                    },
                    None => {},
                }
                self.thread_hand.lock().unwrap().stop();
            }
            "settings" => {
                println!("Got new settings");
                self.count.lock().unwrap().card.set_config(CardConf{trump : data.data.trump, mode : data.data.mode});
            },
            "reset" => {
                println!("Reset counters");
                self.count.lock().unwrap().active_team = CountingTeams::Team0;
                self.count.lock().unwrap().reset();

            },
            _ => {},
        }
    }
}