// Copyright (c) 2016 The Rouille developers
// Licensed under the Apache License, Version 2.0
// <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT
// license <LICENSE-MIT or http://opensource.org/licenses/MIT>,
// at your option. All files in the project carrying such
// notice may not be copied, modified, or distributed except
// according to those terms.
#![allow(dead_code)]
#![allow(unreachable_code)]

extern crate serde;
extern crate serde_json;

#[macro_use]
extern crate serde_derive;

mod team;
mod cards;
mod state_machine;
mod counter;
mod observer;
mod json;
mod thread_handler;
mod process;

extern crate websocket;

#[macro_use]
extern crate log;

use counter::counter::Counter;
use std::sync::Mutex;
use std::sync::Arc;
use std::thread;

use websocket::OwnedMessage;
use websocket::sync::Server;

fn main() {
    let server = Server::bind("0.0.0.0:8000").unwrap();
    let counter = Arc::new(Mutex::new(Counter::new()));

    for request in server.filter_map(Result::ok) {

        let counter_clone = counter.clone();

        thread::Builder::new().name("websocker-server".to_string()).spawn(move || {

            let client = request.accept().unwrap();

            let (mut receiver, sender_raw) = client.split().unwrap();
            let sender = Arc::new(Mutex::new(sender_raw));

            counter_clone.lock().unwrap().update_websocket_transmitter(sender);

            for message in receiver.incoming_messages() {
                let message = message.unwrap();

                match message {
                    OwnedMessage::Close(_) => {
                        return;
                    }
                    OwnedMessage::Text(message) => {
                        counter_clone.lock().unwrap().get_input_data(message.as_str());
                    }
                    _ => println!("Unrecognized request"),
                }
            }
        }).unwrap();
    }
}